#include <conio.h>
#include <graphics.h>

#define High 480  // 游戏画面尺寸
#define Width 640
#define num 10

// 全局变量
int ball_x,ball_y; // 小球的坐标
int ball_vx,ball_vy; // 小球的速度
int radius;  // 小球的半径
int x,y;//挡板的中心坐标
int bar_high,bar_width;//挡板长宽
int left,right,top,botton;//请确定挡板
int zhuan_high,zhuan_width;//砖块长宽
int a[num];//判断砖块是否存在


void startup()  // 数据初始化
{
	ball_x = Width/2;
	ball_y = High/2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 20;
	x=Width/2,y=High-15;
	bar_high=30,bar_width=150;
	left=x-bar_width/2;
	right=x+bar_width/2;
	top=y-bar_high/2;
	botton=y+bar_high/2;
	zhuan_high=30;
	zhuan_width=64;
	int i;
	for(i=0;i<num;i++)
		a[i]=1;

	initgraph(Width, High);
	BeginBatchDraw();
}

void clean()  // 显示画面
{
	// 绘制黑线、黑色填充的圆
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x, ball_y, radius);
	//绘制黑色长方形
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillrectangle(left,top,right,botton);
	//绘制黑色砖块
	int i;
	for(i=0;i<num;i++)
	{
		if(a[i]==0)
		{
			setcolor(BLACK);
			setfillcolor(BLACK);
	        fillrectangle(i*64,0,(i+1)*64,30);
		}
	}
}

void show()  // 显示画面
{
	// 绘制黄线、绿色填充的圆
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillcircle(ball_x, ball_y, radius);
	//绘制挡板
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillrectangle(left,top,right,botton);
	//绘制砖块
	int i;
	for(i=0;i<num;i++)
	{
		if(a[i]==1)
		{
			setcolor(YELLOW);
			setfillcolor(BROWN);
	        fillrectangle(i*64,0,(i+1)*64,30);
		}
	}
	FlushBatchDraw();
	// 延时
	Sleep(3);	
}	

void updateWithoutInput()  // 与用户输入无关的更新
{
	// 更新小圆坐标
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	
	if ((ball_x<=radius)||(ball_x>=Width-radius))
		ball_vx = -ball_vx;
	if ((ball_y<=radius)||(ball_y>=High-radius))
		ball_vy = -ball_vy;	
	//判断挡板是否接住小球
	if(ball_y+radius==top)
	{
		if(ball_x+radius>=left&&ball_x-radius<=right)
		{
			ball_vy = -ball_vy;

		}
	}
	//判断小球是否击中砖块
	int i;
	for(i=0;i<num;i++)
	{
		if(a[i])
		{
			if((ball_y==30)&&(ball_x>=i*64)&&(ball_x<=(i+1)*64))
			{
				a[i]=0;
				ball_vy=-ball_vy;
			}
		}
	}
}


void updateWithInput()  // 与用户输入有关的更新
{

	char input;
	if(kbhit())  // 判断是否有输入
	{
		input = getch();  // 根据用户的不同输入来移动，不必输入回车
		if(input=='a')
		{
			x=x-10;
			left=x-bar_width/2;
			right=x+bar_width/2;
			top=y-bar_high/2;
	        botton=y+bar_high/2;
		}
		if(input=='d')
		{
			x=x+10;
			left=x-bar_width/2;
			right=x+bar_width/2;
			top=y-bar_high/2;
	        botton=y+bar_high/2;
		}
		if(input=='w')
		{
			y=y-10;
			left=x-bar_width/2;
			right=x+bar_width/2;
			top=y-bar_high/2;
        	botton=y+bar_high/2;
		}
		if(input=='s')
		{
			y=y+10;
			left=x-bar_width/2;
			right=x+bar_width/2;
			top=y-bar_high/2;
	        botton=y+bar_high/2;
		}
		
	}

/*	if(MouseHit())
	{
	    MOUSEMSG m;
	    m=GetMouseMsg();
	    if(m.uMsg==WM_MOUSEMOVE)
		{
	    	x=m.x;
	    	y=m.y;
	    	bar_high=30,bar_width=150;
    		left=x-bar_width/2;
    		right=x+bar_width/2;
    		top=y-bar_high/2;
    	    botton=y+bar_high/2;
		}
    	else if(m.uMsg==WM_LBUTTONDOWN)
		{
	    	ball_x=x;
    		ball_y=y-35;
		}
	}
*/	
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		clean();  // 把之前绘制的内容清除
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
		show();  // 显示新画面
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}